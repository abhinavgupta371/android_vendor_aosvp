# Bootanimation
PRODUCT_PACKAGES += \
    bootanimation.zip

# Extra tools
PRODUCT_PACKAGES += \
    openvpn \
    e2fsck \
    mke2fs \
    tune2fs
